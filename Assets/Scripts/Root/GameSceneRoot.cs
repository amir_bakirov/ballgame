﻿using BallGame;
using BallGame.BallCreation;
using BallGame.BallSystems;
using BallGame.BallSystems.MoveSystem;
using BallGame.Camera;
using BallGame.View;
using Common;
using UnityEngine;

namespace Root
{
    public class GameSceneRoot : MonoBehaviour
    {
        [Header("Scene Links")]
        [SerializeField]
        private CameraFieldView _cameraFieldView;

        [SerializeField]
        private TimerView _timerView;

        [SerializeField]
        private ScoreView _scoreView;

        [SerializeField]
        private EndPanelView _endPanelView;

        [Header("Prefabs")]
        [SerializeField]
        private BallView _ballViewPrefab;

        [Header("Configs")]
        [SerializeField]
        private Configs.GameConfig _gameConfig;

        private BallController _ballController;

        public void SimpleBindingDI()
        {
            ISimpleComponentPool<BallView> ballViewPool = new SimpleComponentPool<BallView>(_ballViewPrefab, 50);

            BallMoverWithAcceleration ballMoverWithAcceleration =
                new BallMoverWithAcceleration(_gameConfig.SettingsBallMover);

            BallFactory factory = new BallFactory(ballViewPool);
            BallCreatingSystem ballCreatingSystem = new BallCreatingSystem(factory, _cameraFieldView);
            BallMoveSystem ballMoveSystem = new BallMoveSystem(ballMoverWithAcceleration);
            BallHitSystem ballHitSystem = new BallHitSystem(_cameraFieldView);
            BallRemoveSystem ballRemoveSystem = new BallRemoveSystem(ballViewPool, _cameraFieldView);
            BallUISystem ballUiSystem = new BallUISystem(_scoreView, _timerView, _endPanelView);
            BallScoreSystem ballScoreSystem = new BallScoreSystem();

            _ballController = new BallController(ballCreatingSystem, ballMoveSystem, ballHitSystem,
                ballRemoveSystem, _gameConfig.SettingsBallController, _gameConfig.SettingsBallModel, ballUiSystem,
                ballScoreSystem, ballMoverWithAcceleration);
        }

        // Start is called before the first frame update
        void Start()
        {
            SimpleBindingDI();
            _ballController.Initialize();
        }

        // Update is called once per frame
        void Update()
        {
            _ballController.Tick();
        }
    }
}