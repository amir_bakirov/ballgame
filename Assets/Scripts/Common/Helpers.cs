﻿using System;
using BallGame.BallSystems;
using UnityEngine;
using UnityEngine.Assertions;

namespace Common
{
    public static class Helpers
    {
        public static void CalculateSpawningCubeBounds(Vector3 frustumCubeCenter, Vector3 frustumCubeSize,
            out Vector3 spawningLeftBot, out Vector3 spawningRightTop)
        {
            spawningLeftBot = frustumCubeCenter - frustumCubeSize / 2f +
                              Vector3.down * BallCreatingSystem.SpawningZoneLimitInderCamerasFrustum;
            spawningRightTop = spawningLeftBot + Vector3.right * frustumCubeSize.x -
                               Vector3.down * BallCreatingSystem.SpawningZoneLimitInderCamerasFrustum;
        }

        public static void CalculateRemovingCubeBounds(Vector3 frustumCubeCenter, Vector3 frustumCubeSize,
            out Vector3 removingLeftBot, out Vector3 removingRightTop)
        {
            removingLeftBot = frustumCubeCenter - frustumCubeSize / 2f +
                              Vector3.up * frustumCubeSize.y;
            removingRightTop = removingLeftBot + Vector3.right * frustumCubeSize.x +
                               Vector3.up * BallRemoveSystem.RemoveOffset;
        }

        public static void CalculateFrustumCubeBounds(Camera camera, float distance, out Vector3 frustumCubeSize,
            out Vector3 frustumCubeCenter)
        {
            float frustumHeight = 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
            float frustumWidth = frustumHeight * camera.aspect;

            frustumCubeCenter = camera.transform.position + camera.transform.forward * distance;
            frustumCubeSize = new Vector3(frustumWidth, frustumHeight, 0f);
        }

        public static float Lerp(float a, float b, float f)
        {
            return a + f * (b - a);
        }

        public static void AssertMinMax(Vector2 minMax)
        {
            Assert.IsTrue(minMax.x <= minMax.y, $"Max less than min");
        }

        public static void AssertMinMax(Vector2 minMax, float min, float max)
        {
            AssertMinMax(minMax);
            AssertMinMax(minMax.x, min, max);
            AssertMinMax(minMax.y, min, max);
        }

        public static void AssertMinMax(float value, float min, float max)
        {
            Assert.IsTrue(value >= min && value <= max, $"Size must be in [{min}, {max}]");
        }

        public static void AssertNonNegative(float value)
        {
            Assert.IsTrue(value >= 0, $"Negative value");
        }

        public static void AssertNonNegative(Vector2 vector2)
        {
            AssertNonNegative(vector2.x);
            AssertNonNegative(vector2.y);
        }

        #region HelperClasses

        public class SimpleTimer
        {
            private float _timeToEnd;
            private float _maxTime;

            public SimpleTimer(float timeToEnd) => (_timeToEnd, _maxTime) = (timeToEnd, timeToEnd);

            public float TimeToEnd => _timeToEnd;

            public float MaxTime => _maxTime;

            public Action Callback { get; set; } = null;

            public bool Tick(float timeStep)
            {
                _timeToEnd = TimeToEnd - timeStep;

                var isDone = IsDone();
                if (isDone && Callback != null)
                {
                    Callback.Invoke();
                    Callback = null;
                }

                return isDone;
            }

            public bool IsDone()
            {
                if (TimeToEnd <= 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public float GetCoefToMaxTime()
            {
                if (MaxTime == 0)
                {
                    return 1;
                }

                return 1f - Mathf.Clamp01(TimeToEnd / MaxTime);
            }

            public void Reset()
            {
                _timeToEnd = MaxTime;
            }

            public void SetTimer(float timer)
            {
                _timeToEnd = Mathf.Clamp(timer, 0f, MaxTime);
            }
        }

        #endregion
    }
}