﻿using System.Collections.Generic;
using UnityEngine;

namespace Common
{
    public class SimpleComponentPool<TComponent> : ISimpleComponentPool<TComponent> where TComponent : Component
    {
        private TComponent _prefab;
        private Stack<TComponent> _pool = new Stack<TComponent>();
        private Transform _parent;
    
        public SimpleComponentPool(TComponent prefab, int prewarmSize = 0)
        {
            _prefab = prefab;

            _parent = new GameObject($"Pool of <{typeof(TComponent)}>").transform;

            Prewarm(prewarmSize);
        }

        public void Prewarm(int size)
        {
            for (int i = 0; i < size; i++)
            {
                var instantiate = Component.Instantiate(_prefab, _parent);
                instantiate.gameObject.SetActive(false);
                _pool.Push(instantiate);
            }
        }

        public TComponent Spawn()
        {
            TComponent tComponent;

            if (_pool.Count > 0)
            {
                tComponent = _pool.Pop();
                tComponent.gameObject.SetActive(true);
            }
            else
            {
                tComponent = Component.Instantiate(_prefab, _parent);
            }

            return tComponent;
        }

        public void Despawn(TComponent tComponent)
        {
            _pool.Push(tComponent);
            tComponent.gameObject.SetActive(false);
        }
    }
}