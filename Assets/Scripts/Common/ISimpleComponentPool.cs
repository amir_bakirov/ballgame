﻿using UnityEngine;

namespace Common
{
    public interface ISimpleComponentPool<TComponent> where TComponent : Component
    {
        void Prewarm(int size);
        TComponent Spawn();
        void Despawn(TComponent tComponent);
    }
}