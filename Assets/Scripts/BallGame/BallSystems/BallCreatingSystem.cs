﻿using System;
using BallGame.BallCreation;
using BallGame.Camera;
using Common;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BallGame.BallSystems
{
    public class BallCreatingSystem : IInitializable
    {
        public const float SpawningZoneLimitInderCamerasFrustum = 25f;
        public const float CollisionEps = 0.05f;
        public const float GroundOffset = 0.05f;

        private readonly BallFactory _ballFactory;
        private readonly CameraFieldView _cameraFieldView;

        private SpawnParams _spawnParams = null;

        private bool _initialized = false;

        [Serializable]
        public class SpawnParams
        {
            public Vector3 LeftBotAnchor;
            public Vector3 RightTopAnchor;
        }

        public BallCreatingSystem(BallFactory ballFactory,
            CameraFieldView cameraFieldView)
        {
            _ballFactory = ballFactory;
            _cameraFieldView = cameraFieldView;
        }

        public void Initialize()
        {
            CalculateSpawnParams();
            GenerateGroundForCircleCast();

            _initialized = true;
        }

        public bool SpawnBall(float ballSizeCoef, float speed, float score, Color color, out BallModel ballModel)
        {
            ballModel = null;

            if (_initialized == false)
            {
                Debug.LogError("Need Initialize");
                return false;
            }

            float filedWidth = _spawnParams.RightTopAnchor.x - _spawnParams.LeftBotAnchor.x;
            float ballDiameter = filedWidth * ballSizeCoef;
            var ballRadius = ballDiameter / 2f;
            float zAxis = _spawnParams.RightTopAnchor.z;

            CalculateCircleCast(filedWidth, ballDiameter, ballRadius, out var circleCastOrigin,
                out var circleCastDirection);

            float castDistance = _spawnParams.RightTopAnchor.y - _spawnParams.LeftBotAnchor.y;
            RaycastHit2D raycastHit2D =
                Physics2D.CircleCast(circleCastOrigin, ballRadius, circleCastDirection, castDistance * 2);

            Debug.DrawRay(circleCastOrigin, circleCastDirection * castDistance, Color.yellow, 1f);

            if (raycastHit2D.collider != null)
            {
                // check for filling spawnField(check balls collision), do not spawn in another ball
                var overlapRadius = ballRadius - CollisionEps;
                if (overlapRadius > 0)
                {
                    Collider2D overlapCircle =
                        Physics2D.OverlapCircle(raycastHit2D.centroid, overlapRadius);
                    if (overlapCircle == null)
                    {
                        ballModel = _ballFactory.CreateBall(ballDiameter, speed, score, color);

                        var ballViewTransform = ballModel.BallView.transform;
                        ballViewTransform.position =
                            new Vector3(raycastHit2D.centroid.x, raycastHit2D.centroid.y, zAxis);

                        return true;
                    }
                    else
                    {
                        Debug.LogWarning("skip spawning, have Collision with other ball on spawn on the spawn zone edge");
                        return false;
                    }
                }
                else
                {
                    Debug.LogError($"Ball too small, less than collisionEpsilon");
                    return false;
                }
            }
            else
            {
                Debug.LogError("Collision not found");
                return false;
            }
        }

        private void CalculateCircleCast(float filedWidth, float ballDiameter, float ballRadius,
            out Vector2 circleCastOrigin,
            out Vector2 circleCastDirection)
        {
            float filedWidthForRandom = filedWidth - ballDiameter;
            float randomPointOnWidth = Random.Range(0f, filedWidthForRandom) + ballRadius;

            circleCastOrigin = new Vector2(_spawnParams.LeftBotAnchor.x + randomPointOnWidth,
                _spawnParams.LeftBotAnchor.y);
            circleCastDirection = Vector2.up;
        }

        /// <summary>
        /// Calculates ball Spawn Zone params
        /// </summary>
        private void CalculateSpawnParams()
        {
            Helpers.CalculateFrustumCubeBounds(_cameraFieldView.Camera, _cameraFieldView.Distance,
                out var frustumCubeSize,
                out var frustumCubeCenter);
            Helpers.CalculateSpawningCubeBounds(frustumCubeCenter, frustumCubeSize, out var spawningLeftTop,
                out var spawningRightBot);

            _spawnParams = new SpawnParams() {LeftBotAnchor = spawningLeftTop, RightTopAnchor = spawningRightBot};
        }

        /// <summary>
        /// Generates 2DBoxCollider with size vector2(cameraFrustumWidth, CollisionEps) under Camera Frustum for 2DCircleCast
        /// </summary>
        private void GenerateGroundForCircleCast()
        {
            GameObject go = new GameObject();
            go.name = "2DBallsGround";
            var boxCollider2D = go.AddComponent<BoxCollider2D>();

            var spawnParamsLeftBotAnchor = _spawnParams.LeftBotAnchor;
            var spawnParamsRightTopAnchor = _spawnParams.RightTopAnchor;

            var diagonal = spawnParamsRightTopAnchor - spawnParamsLeftBotAnchor;

            var groundLeftBot = spawnParamsRightTopAnchor + Vector3.left * diagonal.x + Vector3.down * GroundOffset;
            Vector3 boxCenter = (groundLeftBot + spawnParamsRightTopAnchor) / 2f;
            Vector3 boxSize = spawnParamsRightTopAnchor - groundLeftBot;

            go.transform.position = boxCenter;
            go.transform.localScale = boxSize;
        }
    }
}