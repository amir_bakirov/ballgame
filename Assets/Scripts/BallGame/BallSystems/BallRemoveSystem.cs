﻿using System.Collections.Generic;
using BallGame.BallCreation;
using BallGame.Camera;
using Common;
using UnityEngine;

namespace BallGame.BallSystems
{
    public class BallRemoveSystem : IInitializable
    {
        public const float RemoveOffset = 10;

        private readonly ISimpleComponentPool<BallView> _ballViewPool;

        private bool _initialized = false;
        private CameraFieldView _cameraFieldView;
        private float _maxBallHeigth;
        private List<BallModel> _toDelete = new List<BallModel>();

        public BallRemoveSystem(ISimpleComponentPool<BallView> ballViewPool, CameraFieldView cameraFieldView)
        {
            _ballViewPool = ballViewPool;
            _cameraFieldView = cameraFieldView;
        }

        public void Initialize()
        {
            Helpers.CalculateFrustumCubeBounds(_cameraFieldView.Camera, _cameraFieldView.Distance,
                out var frustumCubeSize,
                out var frustumCubeCenter);

            Helpers.CalculateRemovingCubeBounds(frustumCubeCenter, frustumCubeSize, out var removingLeftBot,
                out var removingRightTop);

            _maxBallHeigth = removingRightTop.y;

            _initialized = true;
        }

        public void RemoveBall(List<BallModel> ballModels, int index)
        {
            if (_initialized == false)
            {
                Debug.LogError("Need Initialize");
                return;
            }

            var ballModel = ballModels[index];
            ballModels.RemoveAt(index);
            _ballViewPool.Despawn(ballModel.BallView);
        }

        public void RemoveByRemoveZone(List<BallModel> ballModels)
        {
            if (_initialized == false)
            {
                Debug.LogError("Need Initialize");
                return;
            }

            _toDelete.Clear();

            for (int i = 0; i < ballModels.Count; i++)
            {
                var ballModel = ballModels[i];
                if (ballModel.BallView.transform.position.y > _maxBallHeigth)
                {
                    _toDelete.Add(ballModel);
                }
            }

            for (int i = 0; i < _toDelete.Count; i++)
            {
                var indexOf = ballModels.IndexOf(_toDelete[i]);
                RemoveBall(ballModels, indexOf);
            }
        }
    }
}