﻿using BallGame.Camera;
using UnityEngine;

namespace BallGame.BallSystems
{
    public class BallHitSystem : IInitializable
    {
        private readonly CameraFieldView _cameraFieldView;

        public BallHitSystem(CameraFieldView cameraFieldView)
        {
            _cameraFieldView = cameraFieldView;
        }

        public bool CheckHit(out BallView ballView)
        {
            ballView = null;

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                RaycastHit2D rayHit =
                    Physics2D.GetRayIntersection(_cameraFieldView.Camera.ScreenPointToRay(Input.mousePosition));
                if (rayHit.collider != null)
                {
                    ballView = rayHit.collider.gameObject.GetComponent<BallView>();
                    if (ballView)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void Initialize()
        {
        }
    }
}