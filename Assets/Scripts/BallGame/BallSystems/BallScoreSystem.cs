﻿namespace BallGame.BallSystems
{
    public class BallScoreSystem : IInitializable
    {
        public float ProcessScore(BallModel ballModel, float currentScore)
        {
            return currentScore + ballModel.Score;
        }

        public void Initialize()
        {
        }
    }
}