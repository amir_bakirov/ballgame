﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BallGame.BallSystems.MoveSystem
{
    public class BallMoverWithAcceleration : IBallMover
    {
        [Serializable]
        public class Settings
        {
            public Vector2 SpeedMultiplierMinMax = new Vector2(1, 10);
        }

        private readonly Settings _settings;

        public BallMoverWithAcceleration(Settings settings)
        {
            _settings = settings;
        }

        public float LifeCycleCoef { get; set; } = 1;

        private Vector3 _moveDirection = Vector3.up;

        public void MoveBalls(List<BallModel> ballModels, float timeStep)
        {
            for (int i = 0; i < ballModels.Count; i++)
            {
                var ballModel = ballModels[i];
                float _speedMultiplier = _settings.SpeedMultiplierMinMax.x +
                                         (_settings.SpeedMultiplierMinMax.y - _settings.SpeedMultiplierMinMax.x) *
                                         LifeCycleCoef;
                ballModel.BallView.transform.position += _moveDirection * ballModel.Speed * _speedMultiplier * timeStep;
            }
        }
    }
}