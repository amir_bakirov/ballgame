﻿using System.Collections.Generic;

namespace BallGame.BallSystems.MoveSystem
{
    public interface IBallMover
    {
        void MoveBalls(List<BallModel> ballModels, float timeStep);
    }
}