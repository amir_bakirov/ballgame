﻿using System.Collections.Generic;

namespace BallGame.BallSystems.MoveSystem
{
    public class BallMoveSystem : IInitializable
    {
        private readonly IBallMover _ballMover;

        public BallMoveSystem(IBallMover ballMover)
        {
            _ballMover = ballMover;
        }

        public void MoveBalls(List<BallModel> ballModels, float timeStep)
        {
            _ballMover.MoveBalls(ballModels, timeStep);
        }

        public void Initialize()
        {
        }
    }
}