﻿using System.Globalization;
using BallGame.View;

namespace BallGame.BallSystems
{
    public class BallUISystem : IInitializable
    {
        private readonly ScoreView _scoreView;
        private readonly TimerView _timerView;
        private readonly EndPanelView _endPanelView;

        public BallUISystem(ScoreView scoreView, TimerView timerView, EndPanelView endPanelView)
        {
            _scoreView = scoreView;
            _timerView = timerView;
            _endPanelView = endPanelView;
        }

        public void SetScore(float score)
        {
            var textText = score.ToString("F2", CultureInfo.InvariantCulture);
            _scoreView.Text.text = $"Score: {textText}";
        }

        public void SetTimeToEnd(float time, float maxTime)
        {
            if (time < 0)
            {
                time = 0;
            }

            var timeText = time.ToString("F2", CultureInfo.InvariantCulture);
            var maxTimeText = maxTime.ToString("F2", CultureInfo.InvariantCulture);
            _timerView.Text.text = $"Time: {timeText}/{maxTimeText}";
        }

        public void ShowEndGamePanel(float score)
        {
            _endPanelView.Canvas.enabled = true;
            _endPanelView.FinalScore.text = $"Final Score: {score:F2}";
        }

        public void Initialize()
        {
            _scoreView.Text.text = "Score: 0.00";
            _timerView.Text.text = "";

            _endPanelView.Canvas.enabled = false;
        }
    }
}