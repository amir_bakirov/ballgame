﻿using UnityEngine;
using UnityEngine.UI;

namespace BallGame.View
{
    public class TimerView : MonoBehaviour
    {
        [SerializeField]
        private Canvas _canvas;

        [SerializeField]
        private Text _text;

        public Text Text => _text;

        public Canvas Canvas => _canvas;
    }
}