﻿using UnityEngine;
using UnityEngine.UI;

namespace BallGame.View
{
    public class EndPanelView : MonoBehaviour
    {
        [SerializeField]
        private Canvas _canvas;
        
        [SerializeField]
        private Text _finalScore;

        public Text FinalScore => _finalScore;

        public Canvas Canvas => _canvas;
    }
}