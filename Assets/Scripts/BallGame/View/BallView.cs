﻿using UnityEngine;

namespace BallGame
{
    public class BallView : MonoBehaviour
    {
        [SerializeField]
        private MeshRenderer _meshRenderer;

        public MeshRenderer MeshRenderer => _meshRenderer;
    }
}
