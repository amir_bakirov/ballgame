﻿namespace BallGame
{
    public  interface IInitializable
    {
        void Initialize();
    }
}
