﻿using Common;
using UnityEngine;

namespace BallGame.Camera
{
    public class CameraFieldView : MonoBehaviour
    {
        [SerializeField]
        private float _distance;

        [SerializeField]
        private UnityEngine.Camera _camera;

        public UnityEngine.Camera Camera => _camera;

        public float Distance => _distance;

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Helpers.CalculateFrustumCubeBounds(_camera, _distance, out var frustumCubeSize, out var frustumCubeCenter);

            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(frustumCubeCenter, frustumCubeSize);

            Helpers.CalculateSpawningCubeBounds(frustumCubeCenter, frustumCubeSize, out var spawningLeftBot,
                out var spawningRightTop);
            Vector3 spawningCubeCenter = (spawningRightTop + spawningLeftBot) / 2f;
            Vector3 spawningCubeSize = spawningRightTop - spawningLeftBot;
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(spawningCubeCenter, spawningCubeSize);

            Helpers.CalculateRemovingCubeBounds(frustumCubeCenter, frustumCubeSize, out var removingLeftBot,
                out var removingRightTop);
            Vector3 removinCubeCenter = (removingRightTop + removingLeftBot) / 2f;
            Vector3 removinCubeSize = removingRightTop - removingLeftBot;
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(removinCubeCenter, removinCubeSize);
        }
#endif
    }
}