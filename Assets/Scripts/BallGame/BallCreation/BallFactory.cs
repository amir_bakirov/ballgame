﻿using Common;
using UnityEngine;

namespace BallGame.BallCreation
{
    public class BallFactory
    {
        private readonly ISimpleComponentPool<BallView> _ballViewPool;

        public BallFactory(ISimpleComponentPool<BallView> ballViewPool)
        {
            _ballViewPool = ballViewPool;
        }

        public BallModel CreateBall(float size, float speed, float score, Color color)
        {
            var ballView = _ballViewPool.Spawn();
            ballView.MeshRenderer.material.color = color;

            ballView.transform.localScale = new Vector3(size, size, size);

            BallModel ballModel = new BallModel(ballView, size, speed, score);

            return ballModel;
        }
    }
}