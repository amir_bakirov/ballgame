﻿using System;
using UnityEngine;

namespace BallGame
{
    public class BallModel
    {
        [Serializable]
        public class Settings
        {
            [Tooltip("MinCoef, MaxCoef")]
            public Vector2 SizeMinMaxCoef = new Vector2(0.1f, 0.2f);

            [Tooltip("Min, Max")]
            public Vector2 SpeedMinMax = new Vector2(5f, 10f);

            public Gradient Gradient1, Gradient2;

            public Vector2 ScoreMinMax = new Vector2(10f, 30f);
        }

        public BallView BallView { get; private set; }
        public float Size { get; private set; }
        public float Speed { get; private set; }

        public float Score { get; private set; }

        public BallModel(BallView ballView, float size, float speed, float score) =>
            (BallView, Size, Speed, Score) = (ballView, size, speed, score);
    }
}