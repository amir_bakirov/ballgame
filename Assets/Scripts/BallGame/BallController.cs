﻿using System;
using System.Collections.Generic;
using BallGame.BallSystems;
using BallGame.BallSystems.MoveSystem;
using Common;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BallGame
{
    public class BallController : IInitializable, ITickable
    {
        private readonly BallCreatingSystem _ballCreatingSystem;
        private readonly BallMoveSystem _ballMoveSystem;
        private readonly BallHitSystem _ballHitSystem;
        private readonly BallRemoveSystem _ballRemoveSystem;
        private readonly Settings _settings;
        private readonly BallModel.Settings _ballSettings;
        private readonly BallUISystem _ballUiSystem;
        private readonly BallScoreSystem _ballScoreSystem;
        private readonly BallMoverWithAcceleration _ballMoverWithAcceleration;

        [Serializable]
        public class Settings
        {
            public float StartDelay = 0.6f;
            public float SpawnDelay = 0.5f;
            public float GameSessionTime = 10f;
        }

        private Helpers.SimpleTimer _delayTimer;
        private Helpers.SimpleTimer _spawnTimer;
        private Helpers.SimpleTimer _lifeCycleTimer;

        private List<BallModel> _ballModels = new List<BallModel>();

        private float _score = 0;

        private List<IInitializable> _ballSystems = new List<IInitializable>();

        public BallController(BallCreatingSystem ballCreatingSystem, BallMoveSystem ballMoveSystem,
            BallHitSystem ballHitSystem, BallRemoveSystem ballRemoveSystem, Settings settings,
            BallModel.Settings ballSettings, BallUISystem ballUiSystem, BallScoreSystem ballScoreSystem,
            BallMoverWithAcceleration ballMoverWithAcceleration)
        {
            _ballCreatingSystem = ballCreatingSystem;
            _ballMoveSystem = ballMoveSystem;
            _ballHitSystem = ballHitSystem;
            _ballRemoveSystem = ballRemoveSystem;
            _ballUiSystem = ballUiSystem;
            _ballScoreSystem = ballScoreSystem;

            _ballSystems.Add(_ballCreatingSystem);
            _ballSystems.Add(_ballMoveSystem);
            _ballSystems.Add(_ballHitSystem);
            _ballSystems.Add(_ballRemoveSystem);
            _ballSystems.Add(_ballUiSystem);
            _ballSystems.Add(_ballScoreSystem);

            _settings = settings;
            _ballSettings = ballSettings;

            _ballMoverWithAcceleration = ballMoverWithAcceleration;
        }

        public void Initialize()
        {
            Debug.Log("Start");

            for (int i = 0; i < _ballSystems.Count; i++)
            {
                _ballSystems[i].Initialize();
            }

            _delayTimer = new Helpers.SimpleTimer(_settings.StartDelay);
            _spawnTimer = new Helpers.SimpleTimer(_settings.SpawnDelay);
            //spawn instantly
            _spawnTimer.SetTimer(0f);
            _lifeCycleTimer = new Helpers.SimpleTimer(_settings.GameSessionTime);
            _lifeCycleTimer.Callback = () => { _ballUiSystem.ShowEndGamePanel(_score); };
        }

        public void Tick()
        {
            var timeStep = Time.deltaTime;

            if (_delayTimer.IsDone() == true && _lifeCycleTimer.IsDone() == false)
            {
                _lifeCycleTimer.Tick(timeStep);
                _ballUiSystem.SetTimeToEnd(_lifeCycleTimer.TimeToEnd, _lifeCycleTimer.MaxTime);

                //spawn
                if (_spawnTimer.Tick(timeStep))
                {
                    float randomCoef = Random.Range(0f, 1f);
                    float invertedRandomCoef = 1f - randomCoef;
                    float speed = Helpers.Lerp(_ballSettings.SpeedMinMax.x, _ballSettings.SpeedMinMax.y, randomCoef);
                    float ballSizeCoef = Helpers.Lerp(_ballSettings.SizeMinMaxCoef.x, _ballSettings.SizeMinMaxCoef.y,
                        invertedRandomCoef);
                    float score = Helpers.Lerp(_ballSettings.ScoreMinMax.x, _ballSettings.ScoreMinMax.y, randomCoef);

                    int randomGradient = Random.Range(0, 2);
                    float randomColorValue = Random.Range(0f, 1f);
                    Color color = randomGradient == 0
                        ? _ballSettings.Gradient1.Evaluate(randomColorValue)
                        : _ballSettings.Gradient2.Evaluate(randomColorValue);

                    if (_ballCreatingSystem.SpawnBall(ballSizeCoef, speed, score, color, out var ballModel))
                    {
                        _ballModels.Add(ballModel);
                    }

                    _spawnTimer.Reset();
                }

                //move
                _ballMoverWithAcceleration.LifeCycleCoef = _lifeCycleTimer.GetCoefToMaxTime();
                _ballMoveSystem.MoveBalls(_ballModels, timeStep);

                //hit
                int hittedIndex = -1;
                if (_ballHitSystem.CheckHit(out var ballView))
                {
                    for (int i = 0; i < _ballModels.Count; i++)
                    {
                        var ballModel = _ballModels[i];
                        if (ballModel.BallView == ballView)
                        {
                            hittedIndex = i;
                        }
                    }
                }

                //remove
                if (hittedIndex != -1)
                {
                    _score = _ballScoreSystem.ProcessScore(_ballModels[hittedIndex], _score);
                    _ballUiSystem.SetScore(_score);
                    _ballRemoveSystem.RemoveBall(_ballModels, hittedIndex);
                }

                _ballRemoveSystem.RemoveByRemoveZone(_ballModels);
            }
            else
            {
                _delayTimer.Tick(timeStep);
            }
        }
    }
}