﻿namespace BallGame
{
    public interface ITickable
    {
        void Tick();
    }
}