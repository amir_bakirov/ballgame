﻿using BallGame;
using BallGame.BallSystems;
using BallGame.BallSystems.MoveSystem;
using Common;
using UnityEngine;

namespace Configs
{
    [CreateAssetMenu(fileName = "GameConfig",
        menuName = "Configs/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        [SerializeField]
        private BallModel.Settings _settingsBallModel;

        [SerializeField]
        private BallMoverWithAcceleration.Settings _settingsBallMover;

        [SerializeField]
        private BallController.Settings _settingsBallController;

        public BallModel.Settings SettingsBallModel => _settingsBallModel;

        public BallMoverWithAcceleration.Settings SettingsBallMover => _settingsBallMover;

        public BallController.Settings SettingsBallController => _settingsBallController;

#if UNITY_EDITOR

        private void OnValidate()
        {
            Validate();
        }

        [UnityEditor.CustomEditor(typeof(GameConfig))]
        public class GameConfigCustomEditor : UnityEditor.Editor
        {
            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();

                if (GUILayout.Button("Validate"))
                {
                    (target as GameConfig).Validate();
                }
            }
        }
#endif

        public void Validate()
        {
            //_settingsBallModel
            Helpers.AssertMinMax(_settingsBallModel.SizeMinMaxCoef, 0.01f, 1f);

            Helpers.AssertMinMax(_settingsBallModel.SpeedMinMax);
            Helpers.AssertNonNegative(_settingsBallModel.SpeedMinMax);

            Helpers.AssertMinMax(_settingsBallModel.ScoreMinMax);
            Helpers.AssertNonNegative(_settingsBallModel.ScoreMinMax);

            //_settingsBallMover
            Helpers.AssertMinMax(_settingsBallMover.SpeedMultiplierMinMax);
            Helpers.AssertNonNegative(_settingsBallMover.SpeedMultiplierMinMax);

            //_settingsBallController
            Helpers.AssertNonNegative(_settingsBallController.StartDelay);
            Helpers.AssertNonNegative(_settingsBallController.SpawnDelay);
            Helpers.AssertNonNegative(_settingsBallController.GameSessionTime);
        }
    }
}